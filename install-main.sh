#!/bin/bash

# install base packages

sudo pacman -S --noconfirm xorg lightdm sxhkd firefox rxvt-unicode picom nitrogen lxappearance dmenu nautilus arandr simplescreenrecorder alsa-utils pulseaudio alsa-utils pulseaudio-alsa pavucontrol arc-gtk-theme arc-icon-theme vlc dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji ttf-font-awesome awesome-terminal-fonts rofi playerctl scrot dunst pacman-contrib i3-wm i3lock i3status dmenu thunar feh arandr rofi ttf-droid alsa-utils acpilight udev util-linux i3-gaps xautolock mtpfs git gvfs-mtp  gvfs-gphoto2 xorg-xinit i3blocks feh ttf-font-awesome picom thunar ttf-dorid pacman-contrib pulseaudio-bluetooth pulseaudio-equalizer pulseaudio-jack alsa-utils playerctl xorg-xbacklight xorg-xset acpilight udev util-linux network-manager-applet i3-gaps xautolock

cd ~
mkdir Project
cd Project
git clone https://aur.archlinux.org/jmtpfs.git
cd jmtpfs/
makepkg -s
cd ~
# end

# Install yay
cd ~
cd Project
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
# end

# install aur packages

yay -S --noconfirm i3lock-color polybar siji-git xss-lock mugshot papirus-icon-theme materia-gtk-theme flat-remix-gtk

# end


cd ~

# install zsh
sudo pacman -S --noconfirm zsh git
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/agkozak/zsh-z $ZSH_CUSTOM/plugins/zsh-z
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
chsh -s $(which zsh)
echo "export ZSH=$HOME/.oh-my-zsh
ZSH_THEME='arrow'
source $ZSH/oh-my-zsh.sh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND='fdfind --type f'
export FZF_DEFAULT_OPTS='--layout=reverse --inline-info --height=80%'
plugins=(
    fzf
    git
    history-substring-search
    colored-man-pages
    zsh-autosuggestions
    zsh-syntax-highlighting
    zsh-z
)
clear
neofetch" > .zshrc
# end

# tap to click
mkdir /etc/X11/xorg.conf.d
echo 'Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        Option "Tapping" "on"
EndSection' > /etc/X11/xorg.conf.d/40-libinput.conf
#end
